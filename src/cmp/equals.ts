/*
 * equals.ts
 * Defines a function that compares variables
 * Created on 7/13/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { objEquals } from '../internal';
import { arrayEquals } from '../internal';
import { EqualsOptions } from '../internal';

/**
 * Compares two values and returns whether they are equal
 *
 * @param v1 The first value to compare
 * @param v2 The second value to compare
 * @param opts The comparison options
 *
 * @returns Whether the values are equivalent
 */
export function equals<T>(v1: T, v2: T, opts?: EqualsOptions): boolean {
	//declare the return value
	let ret = true;

	//get the comparison options
	let strict = true;
	let shallow = false;
	if(opts !== undefined) {
		if(opts.strict !== undefined) {
			strict = opts.strict;
		}
		if(opts.shallow !== undefined) {
			shallow = opts.shallow;
		}
	}

	//check the type of the two values
	//and compare them
	if(typeof v1 === "boolean") {
		if(strict) {
			ret = (v1 === v2);
		} else {
			ret = (v1 == v2);
		}
	} else if(typeof v1 === "number") {
		if(strict) {
			ret = (v1 === v2);
		} else {
			ret = (v1 == v2);
		}
	} else if(typeof v1 === "string") {
		if(strict) {
			ret = (v1 === v2);
		} else {
			ret = (v1 == v2);
		}
	} else if(Array.isArray(v1) && Array.isArray(v2)) {
		if(shallow) {
			if(strict) {
				ret = (v1 === v2);
			} else {
				ret = (v1 == v2);
			}
		} else {
			ret = arrayEquals<typeof v1[0]>(v1, v2, opts);
		}
	} else if(typeof v1 === "object") {
		if(shallow) {
			if(strict) {
				ret = (v1 === v2);
			} else {
				ret = (v1 == v2);
			}
		} else {
			ret = objEquals<typeof v1>(v1, v2, opts);
		}
	} 

	//and return the return value
	return ret;
}

//end of file
