/*
 * arrayEquals.ts
 * Defines a function that compares arrays
 * Created on 7/13/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { EqualsOptions } from '../internal';
import { objEquals } from '../internal';

/**
 * Compares two arrays and determines whether
 * they are equivalent.
 *
 * @param arr1 The first array to compare
 * @param arr2 The second arrray to compare
 * @param opts The comparison options
 *
 * @returns Whether `arr1` and `arr2` are equivalent
 */
export function arrayEquals<T>(arr1: T[], 
				arr2: T[], 
				opts?: EqualsOptions): boolean {
	
	//declare the return value
	let ret = true;

	//get the comparison options
	let strict = true;
	let shallow = false;
	if(opts !== undefined) {
		if(opts.strict !== undefined) {
			strict = opts.strict;
		}
		if(opts.shallow !== undefined) {
			shallow = opts.shallow;
		}
	}

	//make sure the arrays have the same length
	if(arr1.length !== arr2.length) {
		return false;
	}

	//loop and compare the arrays
	for(let i = 0; i < arr1.length; i++) {
		//get the current array members
		let m1 = arr1[i];
		let m2 = arr2[i];

		//check the members for null
		if(m1 === null) {
			if(m2 === null) {
				continue;
			} else {
				return false;
			}
		}
		if(m2 === null) {
			if(m1 === null) {
				continue;
			} else {
				return false;
			}
		}

		//compare them
		if(typeof m1 === "boolean") {
			if(strict) {
				ret = (m1 === m2);
			} else {
				ret = (m1 == m2);
			}
		} else if(typeof m1 === "number") {
			if(strict) {
				ret = (m1 === m2);
			} else {
				ret = (m1 == m2);
			}
		} else if(typeof m1 === "string") {
			if(strict) {
				ret = (m1 === m2);
			} else {
				ret = (m1 == m2);
			}
		} else if(Array.isArray(m1) && Array.isArray(m2)) {
			if(shallow) {
				if(strict) {
					ret = (m1 === m2);
				} else {
					ret = (m1 == m2);
				}
			} else {
				ret = arrayEquals<typeof m1[0]>(
						m1, m2, opts
					);
			}
		} else if(typeof m1 === "object") {
			if(shallow) {
				if(strict) {
					ret = (m1 === m2);
				} else {
					ret = (m1 == m2);
				}
			} else {
				ret = objEquals<typeof m1>(m1, m2, opts);
			}
		}

		//and make sure the loop exits if 
		//a comparison is false
		if(ret === false) {
			break;
		}
	}
	
	//and return the return value
	return ret;
}

//end of file
