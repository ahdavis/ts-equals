/*
 * objEquals.ts
 * Defines a function that compares objects
 * Created on 7/13/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { arrayEquals } from '../internal';
import { EqualsOptions } from '../internal';
import { NoObjectError } from '../internal';

/**
 * Returns whether two objects are equivalent
 *
 * @param obj1 The first object to compare
 * @param obj2 The second object to compare
 * @param opts The comparison options
 *
 * @returns Whether the objects are equivalent
 *
 * @throws NoObjectError if non-objects are passed to the function
 */
export function objEquals<T>(obj1: T, 
				obj2: T, 
				opts?: EqualsOptions): boolean {

	//make sure that objects were passed
	if(typeof obj1 !== "object") {
		throw new NoObjectError(obj1);
	}
	if(typeof obj2 !== "object") {
		throw new NoObjectError(obj2);
	}

	//declare the return value
	let ret = true;

	//get the comparison options
	let strict = true;
	let shallow = false;
	if(opts !== undefined) {		
		if(opts.strict !== undefined) {
			strict = opts.strict;
		}
		if(opts.shallow !== undefined) {
			shallow = opts.shallow;
		}
	}

	//make sure the objects have the
	//same number of fields
	if(Object.keys(obj1).length !== Object.keys(obj2).length) {
		return false;
	}

	//loop and compare the objects
	for(let i = 0; i < Object.keys(obj1).length; i++) {
		//get the current fields being compared
		let f1 = obj1[Object.keys(obj1)[i]];
		let f2 = obj2[Object.keys(obj2)[i]];

		//check the fields for null
		if(f1 === null) {
			if(f2 === null) {
				continue;
			} else {
				return false;
			}
		}
		if(f2 === null) {
			if(f1 === null) {
				continue;
			} else {
				return false;
			}
		}

		//compare them
		if(typeof f1 !== "function") {
			if(typeof f1 === "boolean") {
				if(strict) {
					ret = (f1 === f2);
				} else {
					ret = (f1 == f2);
				}
			} else if(typeof f1 === "number") {
				if(strict) {
					ret = (f1 === f2);
				} else {
					ret = (f1 == f2);
				}
			} else if(typeof f1 === "string") {
				if(strict) {
					ret = (f1 === f2);
				} else {
					ret = (f1 == f2);
				}
			} else if(Array.isArray(f1)) {
				if(shallow) {
					if(strict) {
						ret = (f1 === f2);
					} else {
						ret = (f1 == f2);
					}
				} else {
					ret = arrayEquals<typeof f1[0]>(
							f1, f2, opts 
					);
				}
			} else if(typeof f1 === "object") {
				if(shallow) {
					if(strict) {
						ret = (f1 === f2);
					} else {
						ret = (f1 == f2);
					}
				} else {
					ret = objEquals<typeof f1>(
							f1, f2, opts
					);
				}
			}
		}

		//make sure to exit the loop if
		//a comparison is false
		if(ret === false) {
			break;
		}
	}
					
	//and return the return value
	return ret;
}

//end of file
