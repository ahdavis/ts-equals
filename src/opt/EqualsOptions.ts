/*
 * EqualsOptions.ts
 * Defines an interface for comparison options
 * Created on 7/13/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * Configuration options for equality checking
 */
export interface EqualsOptions {
	/**
	 * Defines which type of equality operator
	 * to use for comparisons. If this option
	 * is set to `true`, then the `===` operator
	 * will be used. Otherwise, the `==` operator
	 * will be used. Defaults to `true`.
	 */
	strict?: boolean;

	/**
	 * Defines whether shallow or deep equality
	 * checking is used. "Shallow" means that
	 * non-primitive members of compared objects
	 * will be directly compared. "Deep" means
	 * that non-primitive members will be
	 * recursively compared. Defaults to `false`.
	 */
	shallow?: boolean;
}

//end of file
