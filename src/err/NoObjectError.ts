/*
 * NoObjectError.ts
 * Defines an error that is thrown when non-objects are passed as objects
 * Created on 7/13/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * Thrown when non-objects are passed as objects
 */
export class NoObjectError extends Error {
	//no fields

	/**
	 * Constructs a new NoObjectError instance
	 *
	 * @param badValue The value that triggered the error
	 */
	constructor(badValue: any) {
		super("The value " + badValue.toString() + 
			" is not an object. Its type is: " +
			typeof badValue);
	}
}

//end of file
