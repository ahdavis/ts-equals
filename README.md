# ts-equals

Variable comparison NPM package with TypeScript bindings


# Overview

This package allows for typesafe equality checking of both primitives and
objects in TypeScript or JavaScript. It also provides a mechanism to 
configure how values are compared through the `EqualsOptions` interface.


# Installation

To install this package, simply enter `npm install --save ts-equals` at
the command-line. If you are using TypeScript, you should also enter
`npm install --save-dev @types/ts-equals` at the command-line.


# Usage

To use this package, simply import the `equals` module using your preferred
method of importing JS or TS modules. Next, optionally create an object 
that conforms to the `EqualsOptions` interface (see below). Then, call
the `equals` function with the two values you wish to compare, along with 
your `EqualsOptions` object if you defined one. If you are using 
TypeScript, also pass the type of the objects being compared as a generic
type parameter. The function will then return whether the values are 
equivalent.

# API

The `equals` function is declared as follows:

`function equals<T>(v1: T, v2: T, opts?: EqualsOptions): boolean`

where:

- `T` is the type of the values being compared (TypeScript only)
- `v1` and `v2` are the values being compared
- `opts` is an object that defines the comparison options (optional)

The `EqualsOptions` interface is defined as follows:

```typescript
interface EqualsOptions {
	strict?: boolean;
	shallow?: boolean;
}
```

where:

- `strict` is whether the `==` or `===` operators are used for comparisons
- `shallow` is whether objects and arrays are directly compared

The `strict` option defaults to `true`, while the `shallow` option defaults
to `false`.


# Licensing

This package is completely open source. It is licensed under the Lesser GNU
General Public License, version 3.0. See the `LICENSE` file in the GitLab
repository for more information.


# Contributions

Pull requests and other contributions are greatly appreciated. If you make
a pull request, please make sure that the unit tests pass by running
`npm test` in the root of the repository before submitting your request.
