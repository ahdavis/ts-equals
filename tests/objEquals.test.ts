/*
 * objEquals.test.ts
 * Defines unit tests for the objEquals function
 * Created on 7/13/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { objEquals } from '../src/internal';

//create a class for equality testing
class Foo {
	var1: number;
	var2: boolean;
	var3: string;
	var4: number[];
	var5: Foo | null;

	constructor(v1: number, v2: boolean, 
			v3: string, v4: number[], v5: Foo | null) {
		this.var1 = v1;
		this.var2 = v2;
		this.var3 = v3;
		this.var4 = v4;
		this.var5 = v5;
	}
}

//create test objects
let obj1 = new Foo(0, true, "hello", [4, 6, 8], null);
let obj2 = new Foo(1, false, "world", [3, 5, 7], null);
let obj3 = new Foo(2, true, "salute", [2, 4, 6], obj1);
let obj4 = new Foo(3, false, "terra", [1, 3, 5], obj2);
let obj5 = new Foo(1, false, "world", [3, 5, 7], obj1);

//this test checks object equality
test('Object equality', () => {
	expect(objEquals<Foo>(obj1, obj1)).toBe(true);
});

//this test checks object inequality
test('Object inequality', () => {
	expect(objEquals<Foo>(obj1, obj2)).toBe(false);
});

//this test checks nested null object inequality
test('Nested null object inequality', () => {
	expect(objEquals<Foo>(obj2, obj5)).toBe(false);
});

//this test checks nested object equality
test('Nested object equality', () => {
	expect(objEquals<Foo>(obj3, obj3)).toBe(true);
});

//this test checks nested object inequality
test('Nested object inequality', () => {
	expect(objEquals<Foo>(obj3, obj4)).toBe(false);
});

//end of tests
