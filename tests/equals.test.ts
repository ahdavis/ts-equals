/*
 * equals.test.ts
 * Defines unit tests for the equals function
 * Created on 7/13/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { equals } from '../src/internal';

//define a test class
class Foo {
	private f1: number;
	private f2: string;

	constructor(f1: number, f2: string) {
		this.f1 = f1;
		this.f2 = f2;
	}
}

//define test values
let v1 = 25;
let v2 = 42;
let v3 = "hello";
let v4 = "world";
let v5 = true;
let v6 = false;
let v7 = [1, 2, 3];
let v8 = [4, 5, 6];
let v9 = new Foo(42, "hello");
let v10 = new Foo(56, "world");

//define comparison options
let opts = {
	strict: true,
	shallow: false 
};

//this test checks numerical equality
test('Numerical equality', () => {
	expect(equals<number>(v1, v1, opts)).toBe(true); 
});

//this test checks numerical inequality
test('Numerical inequality', () => {
	expect(equals<number>(v1, v2, opts)).toBe(false);
});

//this test checks string equality
test('String equality', () => {
	expect(equals<string>(v3, v3, opts)).toBe(true); 
});

//this test checks string inequality
test('String inequality', () => {
	expect(equals<string>(v3, v4, opts)).toBe(false);
});

//this test checks boolean equality
test('Boolean equality', () => {
	expect(equals<boolean>(v5, v5, opts)).toBe(true); 
});

//this test checks boolean inequality
test('Boolean inequality', () => {
	expect(equals<boolean>(v5, v6, opts)).toBe(false);
});

//this test checks array equality
test('Array equality', () => {
	expect(equals<Array<number>>(v7, v7, opts)).toBe(true); 
});

//this test checks array inequality
test('Array inequality', () => {
	expect(equals<Array<number>>(v7, v8, opts)).toBe(false);
});

//this test checks object equality
test('Object equality', () => {
	expect(equals<Foo>(v9, v9, opts)).toBe(true); 
});

//this test checks object inequality
test('Object inequality', () => {
	expect(equals<Foo>(v9, v10, opts)).toBe(false);
});

//end of tests
