/*
 * arrayEquals.test.ts
 * Defines unit tests for the arrayEquals function
 * Created on 7/13/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { arrayEquals } from '../src/internal';

//define testing variables
let arr1 = [1, 3, 5, 7, 9];
let arr2 = [2, 4, 6, 8, 10];
let arr3 = ["hello", "world"];
let arr4 = ["salute", "terra"];
let arr5 = [2, 4, [4, 8]];
let arr6 = [3, 5, [6, 10]];
let arr7 = [1, 2, { one: 1, two: 2}];
let arr8 = [2, 4, { two: 2, four: 4}];
let arr9 = [true, false];
let arr10 = [true, true];
let arr11 = [1, 2, { two: 2, four: 4, nums: [5, 6, 7]}];
let arr12 = [2, 4, { four: 4, eight: 8, nums: [9, 10, 11]}];

//define the equality options
let options = {
	strict: true,
	shallow: false 
};

//this test checks numerical array equality
test('Numerical array equality', () => {
	expect(arrayEquals<number>(arr1, arr1, options)).toBe(true);
});

//this test checks numerical array inequality
test('Numerical array inequality', () => {
	expect(arrayEquals<number>(arr1, arr2, options)).toBe(false); 
});

//this test checks string array equality
test('String array equality', () => {
	expect(arrayEquals<string>(arr3, arr3, options)).toBe(true); 
});

//this test checks string array inequality
test('String array inequality', () => {
	expect(arrayEquals<string>(arr3, arr4, options)).toBe(false);
});

//this test checks boolean array equality
test('Boolean array equality', () => {
	expect(arrayEquals<boolean>(arr9, arr9, options)).toBe(true);
});

//this test checks boolean array inequality
test('Boolean array inequality', () => {
	expect(arrayEquals<boolean>(arr9, arr10, options)).toBe(false);
});

//this test checks nested array equality
test('Nested array equality', () => {
	expect(arrayEquals(arr5, arr5, options)).toBe(true);
});

//this test checks nested array inequality
test('Nested array inequality', () => {
	expect(arrayEquals(arr5, arr6, options)).toBe(false);
});

//this test checks nested object equality
test('Nested object equality', () => {
	expect(arrayEquals<any>(arr7, arr7, options)).toBe(true);
});

//this test checks nested object inequality
test('Nested object inequality', () => {
	expect(arrayEquals<any>(arr7, arr8, options)).toBe(false);
});

//this test checks double-nested array equality
test('Double-nested array equality', () => {
	expect(arrayEquals<any>(arr11, arr11, options)).toBe(true);
});

//this test checks double-nested array inequality
test('Double-nested array inequality', () => {
	expect(arrayEquals<any>(arr11, arr12, options)).toBe(false);
});

//end of tests
